/******************************************************************************/
/* Important Spring 2020 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5fd1e93dbf35cbffa3aef28f8c01d8cf2ffc51ef62b26a       */
/*         f9bda5a68e5ed8c972b17bab0f42e24b19daa7bd408305b1f7bd6c7208c1       */
/*         0e36230e913039b3046dd5fd0ba706a624d33dbaa4d6aab02c82fe09f561       */
/*         01b0fd977b0051f0b0ce0c69f7db857b1b5e007be2db6d42894bf93de848       */
/*         806d9152bd5715e9                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "kernel.h"
#include "errno.h"
#include "globals.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/anon.h"

#include "proc/proc.h"

#include "util/debug.h"
#include "util/list.h"
#include "util/string.h"
#include "util/printf.h"

#include "fs/vnode.h"
#include "fs/file.h"
#include "fs/fcntl.h"
#include "fs/vfs_syscall.h"

#include "mm/slab.h"
#include "mm/page.h"
#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/mmobj.h"

static slab_allocator_t *vmmap_allocator;
static slab_allocator_t *vmarea_allocator;

void
vmmap_init(void)
{
        vmmap_allocator = slab_allocator_create("vmmap", sizeof(vmmap_t));
        KASSERT(NULL != vmmap_allocator && "failed to create vmmap allocator!");
        vmarea_allocator = slab_allocator_create("vmarea", sizeof(vmarea_t));
        KASSERT(NULL != vmarea_allocator && "failed to create vmarea allocator!");
}

vmarea_t *
vmarea_alloc(void)
{
        vmarea_t *newvma = (vmarea_t *) slab_obj_alloc(vmarea_allocator);
        if (newvma) {
                newvma->vma_vmmap = NULL;
        }
        return newvma;
}

void
vmarea_free(vmarea_t *vma)
{
        KASSERT(NULL != vma);
        slab_obj_free(vmarea_allocator, vma);
}

/* a debugging routine: dumps the mappings of the given address space. */
size_t
vmmap_mapping_info(const void *vmmap, char *buf, size_t osize)
{
        KASSERT(0 < osize);
        KASSERT(NULL != buf);
        KASSERT(NULL != vmmap);

        vmmap_t *map = (vmmap_t *)vmmap;
        vmarea_t *vma;
        ssize_t size = (ssize_t)osize;

        int len = snprintf(buf, size, "%21s %5s %7s %8s %10s %12s\n",
                           "VADDR RANGE", "PROT", "FLAGS", "MMOBJ", "OFFSET",
                           "VFN RANGE");

        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
                size -= len;
                buf += len;
                if (0 >= size) {
                        goto end;
                }

                len = snprintf(buf, size,
                               "%#.8x-%#.8x  %c%c%c  %7s 0x%p %#.5x %#.5x-%#.5x\n",
                               vma->vma_start << PAGE_SHIFT,
                               vma->vma_end << PAGE_SHIFT,
                               (vma->vma_prot & PROT_READ ? 'r' : '-'),
                               (vma->vma_prot & PROT_WRITE ? 'w' : '-'),
                               (vma->vma_prot & PROT_EXEC ? 'x' : '-'),
                               (vma->vma_flags & MAP_SHARED ? " SHARED" : "PRIVATE"),
                               vma->vma_obj, vma->vma_off, vma->vma_start, vma->vma_end);
        } list_iterate_end();

end:
        if (size <= 0) {
                size = osize;
                buf[osize - 1] = '\0';
        }
        /*
        KASSERT(0 <= size);
        if (0 == size) {
                size++;
                buf--;
                buf[0] = '\0';
        }
        */
        return osize - size;
}

/* Create a new vmmap, which has no vmareas and does
 * not refer to a process. */
vmmap_t *
vmmap_create(void)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_create()\n");
    vmmap_t *newVmmap = (vmmap_t *)slab_obj_alloc(vmmap_allocator);

    //checkthe newVMmap status
    if(newVmmap == NULL){
        dbg(DBG_PRINT, "(GRADING3F) leave vmmap_create() returning newVmmap = NULL\n");
        return NULL;
    }
    else{
        list_init(&newVmmap->vmm_list);
        newVmmap->vmm_proc = NULL;
        dbg(DBG_PRINT, "(GRADING3F) leave vmmap_create() returning newVmmap\n");
        return newVmmap;
    }
}

/* Removes all vmareas from the address space and frees the
 * vmmap struct. */
void
vmmap_destroy(vmmap_t *map)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_destroy()\n");

    KASSERT(NULL != map);
    dbg(DBG_PRINT, "(GRADING3A 3.a)\n");

    if(list_empty(&map->vmm_list)){
        dbg(DBG_PRINT, "(GRADING3F) leave vmmap_destroy() with empty vmm_list\n");
        return;
    }
    else{

        vmarea_t *vma;  

        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {  

            list_remove(&vma->vma_plink); 
            vma->vma_obj->mmo_ops->put(vma->vma_obj);
            list_remove(&vma->vma_olink);           
            vmarea_free(vma);
            
        } list_iterate_end();

    }

    slab_obj_free(vmmap_allocator, map);
    dbg(DBG_PRINT, "(GRADING3F) leave vmmap_destroy()\n");
}

/* Add a vmarea to an address space. Assumes (i.e. asserts to some extent)
 * the vmarea is valid.  This involves finding where to put it in the list
 * of VM areas, and adding it. Don't forget to set the vma_vmmap for the
 * area. */
void
vmmap_insert(vmmap_t *map, vmarea_t *newvma)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_insert()\n");

    KASSERT(NULL != map && NULL != newvma);
    KASSERT(NULL == newvma->vma_vmmap);
    KASSERT(newvma->vma_start < newvma->vma_end);
    KASSERT(ADDR_TO_PN(USER_MEM_LOW) <= newvma->vma_start && ADDR_TO_PN(USER_MEM_HIGH) >= newvma->vma_end);
    dbg(DBG_PRINT, "(GRADING3A 3.b)\n");

    vmarea_t *vma = NULL;
    uint32_t prevEnd = NULL;

    if(list_empty(&map->vmm_list)) {
        list_insert_tail(&map->vmm_list, &newvma->vma_plink);
    }
    else{
        prevEnd = ADDR_TO_PN(USER_MEM_LOW);

        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink){

            if (newvma->vma_start >= prevEnd && newvma->vma_end <= vma->vma_start) {
                list_insert_before(&vma->vma_plink, &newvma->vma_plink);
                newvma->vma_vmmap = map;
                dbg(DBG_PRINT, "(GRADING3F) leave vmmap_insert()\n");
                return;
            } 

            prevEnd = vma->vma_end;
        }list_iterate_end();
        list_insert_tail(&map->vmm_list, &newvma->vma_plink);

    }

    newvma->vma_vmmap = map;
    dbg(DBG_PRINT, "(GRADING3F) leave vmmap_insert()\n");
}

/* Find a contiguous range of free virtual pages of length npages in
 * the given address space. Returns starting vfn for the range,
 * without altering the map. Returns -1 if no such range exists.
 *
 * Your algorithm should be first fit. If dir is VMMAP_DIR_HILO, you
 * should find a gap as high in the address space as possible; if dir
 * is VMMAP_DIR_LOHI, the gap should be as low as possible. */
int
vmmap_find_range(vmmap_t *map, uint32_t npages, int dir)
{

    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_find_range()\n");
    vmarea_t *vma = NULL;

    uint32_t prevStart = ADDR_TO_PN(USER_MEM_HIGH);//from high to low
    uint32_t prevEnd = ADDR_TO_PN(USER_MEM_LOW);// from low to high

    if (dir == VMMAP_DIR_HILO){
        dbg(DBG_PRINT, "(GRADING3F) enter vmmap_find_rnage() dir: VMMAP_DIR_HILO\n");
        list_iterate_reverse(&map->vmm_list, vma, vmarea_t, vma_plink){
            if (prevStart >= npages + vma->vma_end){
                dbg(DBG_PRINT, "(GRADING3F) leave vmmap_find_range() returning starting vfn = %d\n", prevStart - npages);
                return prevStart - npages;
            }
            prevStart = vma->vma_start;

        }list_iterate_end();
        if (prevStart >= npages + ADDR_TO_PN(USER_MEM_LOW)){
            dbg(DBG_PRINT, "(GRADING3F) leave vmmap_find_range() returning starting vfn = %d\n", ADDR_TO_PN(USER_MEM_LOW));
            return prevStart - npages;
        }
    }
    else if(dir == VMMAP_DIR_LOHI){
        dbg(DBG_PRINT, "(GRADING3F) enter vmmap_find_rnage() dir: VMMAP_DIR_LOHI\n");
        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink){
            if (vma->vma_start >= npages + prevEnd) {
                dbg(DBG_PRINT, "(GRADING3F) leave vmmap_find_range() returning starting vfn = %d\n", prevEnd);
                return prevEnd;
            }
            prevEnd = vma->vma_end;
        }list_iterate_end();
        if (prevEnd + npages <= ADDR_TO_PN(USER_MEM_HIGH)) {
            dbg(DBG_PRINT, "(GRADING3F) leave vmmap_find_range() returning starting vfn = %d\n", prevEnd);
            return prevEnd;
        }
    }
    dbg(DBG_PRINT, "(GRADING3F) leave vmmap_find_range() returning -1\n");
    return -1;
}

/* Find the vm_area that vfn lies in. Simply scan the address space
 * looking for a vma whose range covers vfn. If the page is unmapped,
 * return NULL. */
vmarea_t *
vmmap_lookup(vmmap_t *map, uint32_t vfn)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_lookup()\n");
    KASSERT(NULL != map);
    dbg(DBG_PRINT, "(GRADING3A) 3.c\n");
    vmarea_t *vma;

    if(!list_empty(&map->vmm_list)) {
        vma = NULL;
        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
            if(vfn < vma->vma_end && vfn >= vma->vma_start) {
                dbg(DBG_PRINT, "(GRADING3F) leave vmmap_lookup() returing non-NULL vma\n");
                return vma;
            }
        } list_iterate_end();
    }
    dbg(DBG_PRINT, "(GRADING3F) leave vmmap_lookup() returning NULL\n");
    return NULL;
}

/* Allocates a new vmmap containing a new vmarea for each area in the
 * given map. The areas should have no mmobjs set yet. Returns pointer
 * to the new vmmap on success, NULL on failure. This function is
 * called when implementing fork(2). */
vmmap_t *
vmmap_clone(vmmap_t *map)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_clone()\n");
    vmmap_t *newVmmap = vmmap_create();

    if(newVmmap == NULL){
        dbg(DBG_PRINT, "(GRADING3F) leave vmmap_lookup() returning NULL: cannot create vmmap\n");
        return NULL;
    }

    vmarea_t *vma;
    
    list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {

        vmarea_t *newVmar = vmarea_alloc();

        if(newVmar == NULL){
            dbg(DBG_PRINT, "(GRADING3F) leave vmmap_lookup() returning NULL: cannot alloc vmarea\n");
            return NULL;
        }
        
        newVmar->vma_start = vma->vma_start;
        newVmar->vma_end = vma->vma_end;
        newVmar->vma_off = vma->vma_off;
        newVmar->vma_prot = vma->vma_prot;
        newVmar->vma_flags = vma->vma_flags;
        newVmar->vma_obj = NULL;
        list_link_init(&newVmar->vma_plink);
        list_link_init(&newVmar->vma_olink);
        
        vmmap_insert(newVmmap, newVmar);
        newVmar->vma_vmmap = newVmmap;

    } list_iterate_end();
    
    dbg(DBG_PRINT, "(GRADING3F) leave vmmap_clone() returning non-NULL newVmmap\n");
    return newVmmap;
}

/* Insert a mapping into the map starting at lopage for npages pages.
 * If lopage is zero, we will find a range of virtual addresses in the
 * process that is big enough, by using vmmap_find_range with the same
 * dir argument.  If lopage is non-zero and the specified region
 * contains another mapping that mapping should be unmapped.
 *
 * If file is NULL an anon mmobj will be used to create a mapping
 * of 0's.  If file is non-null that vnode's file will be mapped in
 * for the given range.  Use the vnode's mmap operation to get the
 * mmobj for the file; do not assume it is file->vn_obj. Make sure all
 * of the area's fields except for vma_obj have been set before
 * calling mmap.
 *
 * If MAP_PRIVATE is specified set up a shadow object for the mmobj.
 *
 * All of the input to this function should be valid (KASSERT!).
 * See mmap(2) for for description of legal input.
 * Note that off should be page aligned.
 *
 * Be very careful about the order operations are performed in here. Some
 * operation are impossible to undo and should be saved until there
 * is no chance of failure.
 *
 * If 'new' is non-NULL a pointer to the new vmarea_t should be stored in it.
 */
int
vmmap_map(vmmap_t *map, vnode_t *file, uint32_t lopage, uint32_t npages,
          int prot, int flags, off_t off, int dir, vmarea_t **new)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_map()\n");
    KASSERT(NULL != map);
    KASSERT(0 < npages);
    KASSERT((MAP_SHARED & flags) || (MAP_PRIVATE & flags));
    KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_LOW) <= lopage));
    KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_HIGH) >= (lopage + npages)));
    KASSERT(PAGE_ALIGNED(off));
    dbg(DBG_PRINT, "(GRADING3A 3.d)\n");

    vmarea_t *vma = vmarea_alloc();
    mmobj_t *obj;
    int ret;

    if (vma == NULL){
        dbg(DBG_PRINT, "(GRADING3F) leave vmmap_map() returning -ENOMEM\n");
        return -ENOMEM;
    }

    if (lopage != 0){
        int emptyFlag = vmmap_is_range_empty(map, lopage, npages); 
        if (0 == emptyFlag) {
            ret = vmmap_remove(map, lopage, npages);
            if (ret < 0){
                dbg(DBG_PRINT, "(GRADING3F) leave vmmap_map() error on vmmap_remove\n");
                return ret;
            }
        }
        vma->vma_start = lopage;
        vma->vma_end = lopage + npages;
    }
    else {
        ret = vmmap_find_range(map, npages, dir);
        if (ret < 0){
            dbg(DBG_PRINT, "(GRADING3F) leave vmmap_map() error in vmmap_find_range\n");
            return ret;
        }
        vma->vma_start = ret;
        vma->vma_end = ret + npages;
    }

    vma->vma_off = ADDR_TO_PN(off);
    vma->vma_prot = prot;
    vma->vma_flags = flags;
    vma->vma_obj = NULL;
    list_init(&vma->vma_plink);
    list_init(&vma->vma_olink);

    if(file == NULL){   
        //annoymous
        obj = anon_create();
        if (!obj){
            vmarea_free(vma);
            dbg(DBG_PRINT, "(GRADING3F) leave vmmap_map() error in anon_create\n");
            return ret;
        }
    } 
    else {
        //file
        ret = file->vn_ops->mmap(file, vma, &obj);
        if (ret < 0){
            vmarea_free(vma);
            dbg(DBG_PRINT, "(GRADING3F) leave vmmap_map() error in file operations\n");
            return ret;
        }
    }
    
    if(MAP_PRIVATE & flags) {
        //private: create shadow
        mmobj_t *shodow_obj = shadow_create();
        if (!shodow_obj){
            vmarea_free(vma);
            obj->mmo_ops->put(obj);
            return -ENOMEM;
        }
        shodow_obj->mmo_shadowed = obj;
        shodow_obj->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(obj);
        vma->vma_obj = shodow_obj;
    } 
    else {
        //shared
        vma->vma_obj = obj;
    }
    list_insert_tail(&mmobj_bottom_obj(obj)->mmo_un.mmo_vmas, &vma->vma_olink);

    vmmap_insert(map, vma);
    if(new != NULL) {
        *new = vma;
    }
    dbg(DBG_PRINT, "(GRADING3F) leave vmmap_map() successfully\n");
    return 0;
}

/*
 * We have no guarantee that the region of the address space being
 * unmapped will play nicely with our list of vmareas.
 *
 * You must iterate over each vmarea that is partially or wholly covered
 * by the address range [addr ... addr+len). The vm-area will fall into one
 * of four cases, as illustrated below:
 *
 * key:
 *          [             ]   Existing VM Area
 *        *******             Region to be unmapped
 *
 * Case 1:  [   ******    ]
 * The region to be unmapped lies completely inside the vmarea. We need to
 * split the old vmarea into two vmareas. be sure to increment the
 * reference count to the file associated with the vmarea.
 *
 * Case 2:  [      *******]**
 * The region overlaps the end of the vmarea. Just shorten the length of
 * the mapping.
 *
 * Case 3: *[*****        ]
 * The region overlaps the beginning of the vmarea. Move the beginning of
 * the mapping (remember to update vma_off), and shorten its length.
 *
 * Case 4: *[*************]**
 * The region completely contains the vmarea. Remove the vmarea from the
 * list.
 */
int
vmmap_remove(vmmap_t *map, uint32_t lopage, uint32_t npages)
{
        dbg(DBG_PRINT, "(GRADING3F) enter vmmap_remove()\n");
        vmarea_t *vm_area;
        list_iterate_begin(&map->vmm_list, vm_area, vmarea_t, vma_plink)
        {
            if(vm_area->vma_start < lopage && vm_area->vma_end > lopage + npages)
            {
                dbg(DBG_PRINT, "(GRADING3F)vmmap_remove() case 1\n");

                vmarea_t * temp_area = vmarea_alloc();
                if(temp_area == NULL)
                {
                    dbg(DBG_PRINT, "(GRADING3F)vmmap_remove() case 1 no vmarea\n");
                    return -ENOMEM;
                }

                temp_area->vma_start = lopage + npages;
                temp_area->vma_end = vm_area->vma_end;
                temp_area->vma_off = vm_area->vma_off + npages + lopage - vm_area->vma_start;
                temp_area->vma_prot = vm_area->vma_prot;
                temp_area->vma_flags = vm_area->vma_flags;
                temp_area->vma_obj = vm_area->vma_obj;
                list_init(&temp_area->vma_plink);
                list_init(&temp_area->vma_olink);

                temp_area->vma_obj->mmo_ops->ref(temp_area->vma_obj);

                list_insert_before(&vm_area->vma_olink, &temp_area->vma_olink);
                vm_area->vma_end = lopage;

                vmmap_insert(map, temp_area);
            }
            else if(vm_area->vma_start < lopage && vm_area->vma_end <= lopage + npages && lopage < vm_area->vma_end) 
            {
                dbg(DBG_PRINT, "(GRADING3F)vmmap_remove() case 2\n");

                vm_area->vma_end = lopage;
            }
            else if(vm_area->vma_start >= lopage && vm_area->vma_end > lopage + npages && vm_area->vma_start < lopage + npages)
            {
                dbg(DBG_PRINT, "(GRADING3F)vmmap_remove() case 3\n");

                vm_area->vma_off += lopage + npages - vm_area->vma_start;
                vm_area->vma_start = lopage + npages;
            }
            else if(vm_area->vma_start >= lopage && vm_area->vma_end <= lopage + npages)
            {
                dbg(DBG_PRINT, "(GRADING3F)vmmap_remove() case 4\n");

                if(vm_area->vma_obj != NULL)
                {
                    dbg(DBG_PRINT, "(GRADING3F)vmmap_remove() case 4 free mmobj\n");
                    vm_area->vma_obj->mmo_ops->put(vm_area->vma_obj);
                }
                if(list_link_is_linked(&(vm_area->vma_plink))) {
                    list_remove(&(vm_area->vma_plink)); 
                }
                if(list_link_is_linked(&(vm_area->vma_olink))){
                    list_remove(&(vm_area->vma_olink));
                }
                vmarea_free(vm_area);
            }
        }list_iterate_end();

        pt_unmap_range(curproc->p_pagedir, (uintptr_t) PN_TO_ADDR(lopage), (uintptr_t) PN_TO_ADDR(lopage + npages));
        dbg(DBG_PRINT, "(GRADING3F)vmmap_remove() empty list or to right of last\n");
        return 0;
}

/*
 * Returns 1 if the given address space has no mappings for the
 * given range, 0 otherwise.
 */
int
vmmap_is_range_empty(vmmap_t *map, uint32_t startvfn, uint32_t npages)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_is_range_empty()\n");
    uint32_t endvfn = startvfn + npages;
    KASSERT((startvfn < endvfn) && (ADDR_TO_PN(USER_MEM_LOW) <= startvfn) && (ADDR_TO_PN(USER_MEM_HIGH) >= endvfn));/* the specified page range must not be empty and lie completely within the user space */
    dbg(DBG_PRINT, "(GRADING3A 3.e)\n");

    if (list_empty(&map->vmm_list)){
        dbg(DBG_PRINT, "(GRADING3F) empty list, return 1\n");
        return 1;
    }
    vmarea_t *vm_area;
    list_iterate_begin(&map->vmm_list, vm_area, vmarea_t, vma_plink) {
    if(vm_area->vma_start >= endvfn)
    {
        dbg(DBG_PRINT, "(GRADING3F)vmmap_is_range_empty() success, return 1\n");
        return 1;
    }
    if(vm_area->vma_end > startvfn)
    {
        dbg(DBG_PRINT, "(GRADING3F)vmmap_is_range_empty() fail, reutrn 0\n");
        return 0;
    }
}list_iterate_end();
    dbg(DBG_PRINT, "(GRADING3F)vmmap_is_range_empty() success, reutrn 1\n");
    return 1;
}

/* Read into 'buf' from the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do so, you will want to find the vmareas
 * to read from, then find the pframes within those vmareas corresponding
 * to the virtual addresses you want to read, and then read from the
 * physical memory that pframe points to. You should not check permissions
 * of the areas. Assume (KASSERT) that all the areas you are accessing exist.
 * Returns 0 on success, -errno on error.
 */
int
vmmap_read(vmmap_t *map, const void *vaddr, void *buf, size_t count)
{
    dbg(DBG_PRINT, "(GRADING3F)enter vmmap_read()\n");
    vmarea_t *vm_area;
    uintptr_t bytes_num;
    uint32_t start_v = ADDR_TO_PN(vaddr);
    uintptr_t start_address_frame;
    uintptr_t offset;
    vm_area = vmmap_lookup(map, start_v);
    uint32_t start_p = vm_area->vma_off + (start_v - vm_area->vma_start);
    pframe_t *pframe = NULL;
    pframe_get(vm_area->vma_obj, start_p, &pframe);
    if(PAGE_SIZE > PAGE_OFFSET(vaddr) + count){
        bytes_num = count;
    }else{
        bytes_num = PAGE_SIZE - PAGE_OFFSET(vaddr);
    }
    start_address_frame = (uintptr_t)(pframe->pf_addr) + PAGE_OFFSET(vaddr);
    memcpy((void *) buf, (void *)start_address_frame, bytes_num);
    start_v++;
    count = count - bytes_num;
    offset = bytes_num;
    uint32_t i = ADDR_TO_PN(vaddr);
    while(count > 0){
        while(start_v < vm_area->vma_end){
            start_p = vm_area->vma_off + (start_v - vm_area->vma_start);
            pframe_get(vm_area->vma_obj, start_p, &pframe);
            if(count < PAGE_SIZE){
                bytes_num = count;
            }
            start_address_frame = (uintptr_t) (pframe->pf_addr);
            memcpy((void *) ((uintptr_t) buf + offset), (void *) start_address_frame, bytes_num);
            offset += bytes_num;
            count -= bytes_num;
            start_v++;
        }
        vm_area = list_item(vm_area->vma_plink.l_next, vmarea_t, vma_plink);
        start_v = vm_area->vma_start;
    }
    dbg(DBG_PRINT, "(GRADING3F) leave vmmap_read()\n");
    return 0;
}

/* Write from 'buf' into the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do this, you will need to find the correct
 * vmareas to write into, then find the correct pframes within those vmareas,
 * and finally write into the physical addresses that those pframes correspond
 * to. You should not check permissions of the areas you use. Assume (KASSERT)
 * that all the areas you are accessing exist. Remember to dirty pages!
 * Returns 0 on success, -errno on error.
 */
int
vmmap_write(vmmap_t *map, void *vaddr, const void *buf, size_t count)
{
    dbg(DBG_PRINT, "(GRADING3F) enter vmmap_write()\n");
    uint32_t start_v;
    uint32_t start_p;
    uintptr_t bytes_num;
    uintptr_t start_address_frame;
    uintptr_t offset;
    vmarea_t *vm_area;
    start_v = ADDR_TO_PN(vaddr);
    vm_area = vmmap_lookup(map, start_v);
    uint32_t i;
    int flag = 1;
    pframe_t *pframe = NULL;
    while(count > 0){
        if(flag){
            flag = 0;
            start_p = vm_area->vma_off + (start_v - vm_area->vma_start);
            int err = pframe_get(vm_area->vma_obj, start_p, &pframe);
            if(err < 0)
            {
                dbg(DBG_PRINT, "(GRADING3F)vmmap_write() fail first writing no pframe\n");
                return err;
            }
            bytes_num = MIN((PAGE_SIZE - PAGE_OFFSET(vaddr)), count);
            start_address_frame = (uintptr_t) (pframe->pf_addr) + PAGE_OFFSET(vaddr);
            memcpy((void *) start_address_frame, (void *) buf, bytes_num);
            start_v++;
            count = count - bytes_num;
            offset = bytes_num;
            i = ADDR_TO_PN(vaddr);
        }else{
            while(start_v < vm_area->vma_end){
                start_p = vm_area->vma_off + (start_v - vm_area->vma_start);
                pframe_lookup(vm_area->vma_obj, start_p, 1, &pframe);
                bytes_num = count;
                uintptr_t buf_addr = (uintptr_t) buf + offset;
                start_address_frame = (uintptr_t) (pframe->pf_addr);
                memcpy((void *)start_address_frame, (void *) buf_addr, bytes_num);
                start_v++;
                offset = offset + bytes_num;
                count = count - bytes_num;
            }
            i++;
            vm_area = vmmap_lookup(map, i);
            start_v = vm_area->vma_start;
        }
    }
    dbg(DBG_PRINT, "(GRADING3F)vmmap_write() finish\n");
    return 0;
}
