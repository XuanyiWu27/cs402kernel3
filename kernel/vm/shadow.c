/******************************************************************************/
/* Important Spring 2020 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5fd1e93dbf35cbffa3aef28f8c01d8cf2ffc51ef62b26a       */
/*         f9bda5a68e5ed8c972b17bab0f42e24b19daa7bd408305b1f7bd6c7208c1       */
/*         0e36230e913039b3046dd5fd0ba706a624d33dbaa4d6aab02c82fe09f561       */
/*         01b0fd977b0051f0b0ce0c69f7db857b1b5e007be2db6d42894bf93de848       */
/*         806d9152bd5715e9                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "globals.h"
#include "errno.h"

#include "util/string.h"
#include "util/debug.h"

#include "mm/mmobj.h"
#include "mm/pframe.h"
#include "mm/mm.h"
#include "mm/page.h"
#include "mm/slab.h"
#include "mm/tlb.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/shadowd.h"

#define SHADOW_SINGLETON_THRESHOLD 5

int shadow_count = 0; /* for debugging/verification purposes */
#ifdef __SHADOWD__
/*
 * number of shadow objects with a single parent, that is another shadow
 * object in the shadow objects tree(singletons)
 */
static int shadow_singleton_count = 0;
#endif

static slab_allocator_t *shadow_allocator;

static void shadow_ref(mmobj_t *o);
static void shadow_put(mmobj_t *o);
static int  shadow_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf);
static int  shadow_fillpage(mmobj_t *o, pframe_t *pf);
static int  shadow_dirtypage(mmobj_t *o, pframe_t *pf);
static int  shadow_cleanpage(mmobj_t *o, pframe_t *pf);

static mmobj_ops_t shadow_mmobj_ops = {
        .ref = shadow_ref,
        .put = shadow_put,
        .lookuppage = shadow_lookuppage,
        .fillpage  = shadow_fillpage,
        .dirtypage = shadow_dirtypage,
        .cleanpage = shadow_cleanpage
};

/*
 * This function is called at boot time to initialize the
 * shadow page sub system. Currently it only initializes the
 * shadow_allocator object.
 */
void
shadow_init()
{
        dbg(DBG_PRINT, "(GRADING3F) enter shadow_init()\n");
        shadow_allocator = slab_allocator_create("shadow_init", sizeof(mmobj_t));
        KASSERT(shadow_allocator);
        dbg(DBG_PRINT, "(GRADING3A 6.a)\n");
        dbg(DBG_PRINT, "(GRADING3F) leave shadow_init()\n");
}

/*
 * You'll want to use the shadow_allocator to allocate the mmobj to
 * return, then then initialize it. Take a look in mm/mmobj.h for
 * macros or functions which can be of use here. Make sure your initial
 * reference count is correct.
 */
mmobj_t *
shadow_create()
{
    dbg(DBG_PRINT, "(GRADING3F) enter shadow_create()\n");
    mmobj_t *shw_obj = (mmobj_t*)slab_obj_alloc(shadow_allocator);
    mmobj_init(shw_obj, &shadow_mmobj_ops);
    shw_obj->mmo_refcount++;
    dbg(DBG_PRINT, "(GRADING3F) leave shadow_create()\n");
    return shw_obj;
}

/* Implementation of mmobj entry points: */

/*
 * Increment the reference count on the object.
 */
static void
shadow_ref(mmobj_t *o)
{
        dbg(DBG_PRINT, "(GRADING3F) enter shadow_ref()\n");
        KASSERT(o && (0 < o->mmo_refcount) && (&shadow_mmobj_ops == o->mmo_ops));
        dbg(DBG_PRINT, "(GRADING3A 6.b)\n");
        o->mmo_refcount++;
        dbg(DBG_PRINT, "(GRADING3F) leave shadow_ref()\n");
}

/*
 * Decrement the reference count on the object. If, however, the
 * reference count on the object reaches the number of resident
 * pages of the object, we can conclude that the object is no
 * longer in use and, since it is a shadow object, it will never
 * be used again. You should unpin and uncache all of the object's
 * pages and then free the object itself.
 */
static void
shadow_put(mmobj_t *o)
{
        dbg(DBG_PRINT, "(GRADING3F) enter shadow_put()\n");
        KASSERT(o && (0 < o->mmo_refcount) && (&shadow_mmobj_ops == o->mmo_ops));
        dbg(DBG_PRINT, "(GRADING3A 6.c)\n");

        // pframe_t * pf_t;
        // if(o->mmo_refcount==o->mmo_nrespages + 1){
        //         list_iterate_begin(&(o->mmo_respages),pf_t,pframe_t,pf_olink){
        //             if (pframe_is_pinned(pf_t))
        //                 pframe_unpin(pf_t);
        //             pframe_free(pf_t);
        //     }list_iterate_end();
        // }
        // o->mmo_refcount = o->mmo_refcount-1;
        // if(0 == o->mmo_refcount){
        //         o->mmo_shadowed->mmo_ops->put(o->mmo_shadowed);
        //         slab_obj_free(shadow_allocator, o);
        // }

        //change recursion to loop

        mmobj_t *cur = o;
        for(; cur->mmo_shadowed != NULL; ){

            if (cur->mmo_nrespages != cur->mmo_refcount - 1){
                cur->mmo_refcount--;
                dbg(DBG_PRINT,"(GRADING3F) leave shadow_put: just decrease a refcount\n");
                return;
            }

            //free pframe cause it reaches the respages
            pframe_t* pf;
            list_iterate_begin(&(cur->mmo_respages),pf,pframe_t,pf_olink){
                     if (pframe_is_pinned(pf))
                         pframe_unpin(pf);
                     pframe_free(pf);
                     dbg(DBG_PRINT,"(GRADING3F) free one page frame\n");
             }list_iterate_end();

             cur->mmo_refcount --;

             mmobj_t * next = cur->mmo_shadowed;
             //free shadow obj
             slab_obj_free(shadow_allocator, cur);
             cur = next;
        }
        //dont forget to free anon obj or vnodes!
        cur->mmo_ops->put(cur);

        dbg(DBG_PRINT, "(GRADING3F) leave shadow_put() with free shadow objects\n");
}

/* This function looks up the given page in this shadow object. The
 * forwrite argument is true if the page is being looked up for
 * writing, false if it is being looked up for reading. This function
 * must handle all do-not-copy-on-not-write magic (i.e. when forwrite
 * is false find the first shadow object in the chain which has the
 * given page resident). copy-on-write magic (necessary when forwrite
 * is true) is handled in shadow_fillpage, not here. It is important to
 * use iteration rather than recursion here as a recursive implementation
 * can overflow the kernel stack when looking down a long shadow chain */
static int
shadow_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf)
{
    pframe_t* pf_t;
    mmobj_t *temp = o;
    dbg(DBG_PRINT, "(GRADING3F) enter shadow_lookuppage()\n");
    int res;
        while(1){
                if (0 != forwrite){
                       res = pframe_get(temp, pagenum, &pf_t);
                        break;
                }
                if(NULL == temp->mmo_shadowed){
                        res = mmobj_bottom_obj(o)->mmo_ops->lookuppage(mmobj_bottom_obj(o), pagenum, forwrite, &pf_t);
                        if (NULL == pf_t){
                            dbg(DBG_PRINT, "(GRADING3F) leave shadow_lookuppage() returning pf_t == NULL\n");
                            return -1;
                        }
                        break;

                }
                pf_t = pframe_get_resident(temp, pagenum);
                if(NULL != pf_t){
                    res = 0;
                    break;
                }else{
                    temp = temp->mmo_shadowed;
                }
        }
    if(pf_t == NULL){
        dbg(DBG_PRINT, "(GRADING3F) leave shadow_lookuppage() pf_t == NULL\n");
        return -1;
    }
    *pf = pf_t;
    KASSERT(NULL != (*pf));
    dbg(DBG_PRINT, "(GRADING3A 6.d)\n");
    KASSERT((pagenum == (*pf)->pf_pagenum) && (!pframe_is_busy(*pf)));
    dbg(DBG_PRINT, "(GRADING3A 6.d)\n");
    dbg(DBG_PRINT, "(GRADING3F) leave shadow_lookuppage()\n");

    return res;
}

/* As per the specification in mmobj.h, fill the page frame starting
 * at address pf->pf_addr with the contents of the page identified by
 * pf->pf_obj and pf->pf_pagenum. This function handles all
 * copy-on-write magic (i.e. if there is a shadow object which has
 * data for the pf->pf_pagenum-th page then we should take that data,
 * if no such shadow object exists we need to follow the chain of
 * shadow objects all the way to the bottom object and take the data
 * for the pf->pf_pagenum-th page from the last object in the chain).
 * It is important to use iteration rather than recursion here as a
 * recursive implementation can overflow the kernel stack when
 * looking down a long shadow chain */
static int
shadow_fillpage(mmobj_t *o, pframe_t *pf)
{
    dbg(DBG_PRINT, "(GRADING3F) enter shadow_fillpage()\n");
    KASSERT(pframe_is_busy(pf));
    dbg(DBG_PRINT, "(GRADING3A 6.e)\n");
    KASSERT(!pframe_is_pinned(pf));
    dbg(DBG_PRINT, "(GRADING3A 6.e)\n");

    pframe_t *pt_obj;
    int ret = shadow_lookuppage(o->mmo_shadowed, pf->pf_pagenum, 0, &pt_obj);
    if (ret == 0){//successful
        memcpy(pf->pf_addr,pt_obj->pf_addr,PAGE_SIZE);
        pframe_pin(pf);
    }
    
    dbg(DBG_PRINT, "(GRADING3F) leave shadow_fillpage() with ret = %d\n",ret);
    return ret;
}

/* These next two functions are not difficult. */

static int
shadow_dirtypage(mmobj_t *o, pframe_t *pf)
{
    dbg(DBG_PRINT, "(GRADING3F) enter shadow_dirtypage()\n");
    dbg(DBG_PRINT, "(GRADING3F) leave shadow_dirtypage()\n");
    return 0;
}

static int
shadow_cleanpage(mmobj_t *o, pframe_t *pf)
{
    dbg(DBG_PRINT, "(GRADING3F) enter shadow_cleanpage()\n");
    dbg(DBG_PRINT, "(GRADING3F) leave shadow_cleanpage()\n");
    return 0;
}
