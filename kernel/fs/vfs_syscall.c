/******************************************************************************/
/* Important Spring 2020 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5fd1e93dbf35cbffa3aef28f8c01d8cf2ffc51ef62b26a       */
/*         f9bda5a68e5ed8c972b17bab0f42e24b19daa7bd408305b1f7bd6c7208c1       */
/*         0e36230e913039b3046dd5fd0ba706a624d33dbaa4d6aab02c82fe09f561       */
/*         01b0fd977b0051f0b0ce0c69f7db857b1b5e007be2db6d42894bf93de848       */
/*         806d9152bd5715e9                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 *  FILE: vfs_syscall.c
 *  AUTH: mcc | jal
 *  DESC:
 *  DATE: Wed Apr  8 02:46:19 1998
 *  $Id: vfs_syscall.c,v 1.2 2018/05/27 03:57:26 cvsps Exp $
 */

#include "kernel.h"
#include "errno.h"
#include "globals.h"
#include "fs/vfs.h"
#include "fs/file.h"
#include "fs/vnode.h"
#include "fs/vfs_syscall.h"
#include "fs/open.h"
#include "fs/fcntl.h"
#include "fs/lseek.h"
#include "mm/kmalloc.h"
#include "util/string.h"
#include "util/printf.h"
#include "fs/stat.h"
#include "util/debug.h"

/*
 * Syscalls for vfs. Refer to comments or man pages for implementation.
 * Do note that you don't need to set errno, you should just return the
 * negative error code.
 */

/* To read a file:
 *      o fget(fd)
 *      o call its virtual read vn_op
 *      o update f_pos
 *      o fput() it
 *      o return the number of bytes read, or an error
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not a valid file descriptor or is not open for reading.
 *      o EISDIR
 *        fd refers to a directory.
 *
 * In all cases, be sure you do not leak file refcounts by returning before
 * you fput() a file that you fget()'ed.
 */
int
do_read(int fd, void *buf, size_t nbytes)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_read() with fd = %d\n",fd);
        file_t* file = fget(fd);
        int ret;
        if (file == NULL){
                dbg(DBG_PRINT, "(GRADING2D) leave do_read() with error: not valid file descriptor, return -EBADF\n");
                return -EBADF;
        }
        if (!(file->f_mode & FMODE_READ)){
                fput(file);
                dbg(DBG_PRINT, "(GRADING2D) leave do_read() with error: file mode is not for reading, return -EBADF\n");
                return -EBADF;
        }
        if (S_ISDIR(file->f_vnode->vn_mode)){
                fput(file);
                dbg(DBG_PRINT, "(GRADING2D) leave do_read() with error: file is a directory, return -EISDIR\n");
                return -EISDIR;
        }

        ret = file->f_vnode->vn_ops->read(file->f_vnode, file->f_pos, buf, nbytes);
        do_lseek(fd, ret, SEEK_CUR);
        fput(file);
        dbg(DBG_PRINT, "(GRADING2D) leave do_read() successfully with %d bytes read\n", ret);
        return ret;
        /*NOT_YET_IMPLEMENTED("VFS: do_read");
        return -1;*/
}

/* Very similar to do_read.  Check f_mode to be sure the file is writable.  If
 * f_mode & FMODE_APPEND, do_lseek() to the end of the file, call the write
 * vn_op, and fput the file.  As always, be mindful of refcount leaks.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not a valid file descriptor or is not open for writing.
 */
int
do_write(int fd, const void *buf, size_t nbytes)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_write() with fd = %d\n",fd);
        file_t* file = fget(fd);
        int ret;
        if (file == NULL){
                 dbg(DBG_PRINT, "(GRADING2D) leave do_write() with error: not valid file descriptor, return -EBADF\n");
                return -EBADF;
        }
        if (!((file->f_mode & FMODE_WRITE) || (file->f_mode & FMODE_APPEND))){
                fput(file);
                dbg(DBG_PRINT, "(GRADING2D) leave do_write() with error: file mode is not for writing, return -EBADF\n");
                return -EBADF;
        }
        if (S_ISDIR(file->f_vnode->vn_mode)){
                fput(file);
                dbg(DBG_PRINT, "(GRADING2D) leave do_write() with error: file is a directory, return -EISDIR\n");
                return -EISDIR;
        }
        if (file->f_mode & FMODE_APPEND){// write from SEEK_END
                do_lseek(fd, 0, SEEK_END);
        }
        //write from SEEK_CUR, FMODE_WRITE
        ret = file->f_vnode->vn_ops->write(file->f_vnode, file->f_pos, buf, nbytes);
        do_lseek(fd, ret, SEEK_CUR);

        KASSERT((S_ISCHR(file->f_vnode->vn_mode)) || (S_ISBLK(file->f_vnode->vn_mode)) || ((S_ISREG(file->f_vnode->vn_mode)) && (file->f_pos <= file->f_vnode->vn_len))); /* cursor must not go past end of file for these file types */
        dbg(DBG_PRINT, "(GRADING2A 3.a)\n");

        fput(file);
        dbg(DBG_PRINT, "(GRADING2D) leave do_write() successfully with %d bytes written\n", ret);
        //NOT_YET_IMPLEMENTED("VFS: do_write");
        return ret;
}

/*
 * Zero curproc->p_files[fd], and fput() the file. Return 0 on success
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd isn't a valid open file descriptor.
 */
int
do_close(int fd)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_close() with fd = %d\n", fd);
        if (fd < 0 || fd > NFILES || NULL == curproc->p_files[fd]){

                dbg(DBG_PRINT, "(GRADING2D) leave do_close() with error: invalid file descriptor %d\n", fd);
                return -EBADF;
        }

        //sucess
        fput(curproc->p_files[fd]);
        curproc->p_files[fd] = NULL;
        //NOT_YET_IMPLEMENTED("VFS: do_close");
        dbg(DBG_PRINT, "(GRADING2D) leave do_close() successfully\n");
        return 0;
}

/* To dup a file:
 *      o fget(fd) to up fd's refcount
 *      o get_empty_fd()
 *      o point the new fd to the same file_t* as the given fd
 *      o return the new file descriptor
 *
 * Don't fput() the fd unless something goes wrong.  Since we are creating
 * another reference to the file_t*, we want to up the refcount.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd isn't an open file descriptor.
 *      o EMFILE
 *        The process already has the maximum number of file descriptors open
 *        and tried to open a new one.
 */
int
do_dup(int fd)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_dup() with fd = %d\n", fd);
        if (fd < 0 || fd > NFILES){
                dbg(DBG_PRINT, "(GRADING2D) leave do_dup() with error: invalid file descriptor %d\n", fd);
                return -EBADF;
        }
        file_t * file = fget(fd);
        if (file == NULL){
                dbg(DBG_PRINT, "(GRADING2D) leave do_dup() with error: invalid file descriptor %d\n", fd);
                return -EBADF;
        }
        int nfd = get_empty_fd(curproc);
        if (nfd == -EMFILE){
                dbg(DBG_PRINT, "(GRADING2D) leave do_dup() with error: maximum number of file descriptors\n");
                fput(file);
                return nfd;
        }
        //success
        curproc->p_files[nfd] = file;
        dbg(DBG_PRINT, "(GRADING2D) leave do_dup() successfully with nfd = %d\n", nfd);
        //NOT_YET_IMPLEMENTED("VFS: do_dup");
        return nfd;
}

/* Same as do_dup, but insted of using get_empty_fd() to get the new fd,
 * they give it to us in 'nfd'.  If nfd is in use (and not the same as ofd)
 * do_close() it first.  Then return the new file descriptor.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        ofd isn't an open file descriptor, or nfd is out of the allowed
 *        range for file descriptors.
 */
int
do_dup2(int ofd, int nfd)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_dup2() with ofd = %d, nfd = %d\n", ofd, nfd);
        if (ofd < 0 || ofd > NFILES){
                dbg(DBG_PRINT, "(GRADING2D) leave do_dup2() with error: invalid file descriptor %d\n", ofd);
                return -EBADF;
        }
        if (nfd < 0 || nfd > NFILES){
                dbg(DBG_PRINT, "(GRADING2D) leave do_dup2() with error: invalid file descriptor %d\n", nfd);
                return -EBADF;
        }
        
        file_t * file = fget(ofd);

        if (file == NULL){
                dbg(DBG_PRINT, "(GRADING2D) leave do_dup2() with error: invalid file descriptor %d\n", ofd);
                return -EBADF;
        }
        if (ofd == nfd){
                dbg(DBG_PRINT, "(GRADING2D) leave do_dup2() with ofd equal to nfd: %d\n", nfd);
                fput(file);
                return nfd;
        }

        if (curproc->p_files[nfd] != NULL){
                do_close(nfd);
                dbg(DBG_PRINT, "(GRADING2D) nfd has a opened file, close it\n");
        }
        //sucess
        curproc->p_files[nfd] = file;
        //NOT_YET_IMPLEMENTED("VFS: do_dup2");
        dbg(DBG_PRINT, "(GRADING2D) leave do_dup2() successfully with nfd = %d\n", nfd);
        return nfd;
}

/*
 * This routine creates a special file of the type specified by 'mode' at
 * the location specified by 'path'. 'mode' should be one of S_IFCHR or
 * S_IFBLK (you might note that mknod(2) normally allows one to create
 * regular files as well-- for simplicity this is not the case in Weenix).
 * 'devid', as you might expect, is the device identifier of the device
 * that the new special file should represent.
 *
 * You might use a combination of dir_namev, lookup, and the fs-specific
 * mknod (that is, the containing directory's 'mknod' vnode operation).
 * Return the result of the fs-specific mknod, or an error.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EINVAL
 *        mode requested creation of something other than a device special
 *        file.
 *      o EEXIST
 *        path already exists.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_mknod(const char *path, int mode, unsigned devid)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_mkhod()\n");
        if (!S_ISCHR(mode) && !S_ISBLK(mode)){
                dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() with error: invaild mode\n");
                return -EINVAL;
        }
        size_t namelen;
        const char * filename;
        vnode_t * dir_vnode;
        vnode_t * file_vnode;

        int ret = dir_namev(path, &namelen, &filename, NULL, &dir_vnode);

        if (ret < 0){//ENOENT || ENAMETOOLONG
                dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() with error: error number return from dir_namev()\n");
                return ret;                
        }

        ret = lookup(dir_vnode, filename, namelen, &file_vnode);

        if (ret == 0){//lookup success, filename exists
                vput(file_vnode);
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() with error: path already exists\n");
                return -EEXIST;
        }
        if (ret == -ENOTDIR){
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() with error: some components in path is not directory\n");
                return -ENOTDIR;
        }
        if (ret == -ENAMETOOLONG){
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() with error: some components name in path is too long\n");
                return -ENAMETOOLONG;
        }
        if (ret == -ENOENT){ /* dir_vnode is the directory vnode where you will create the target special file */
                KASSERT(NULL != dir_vnode->vn_ops->mknod);
                dbg(DBG_PRINT, "(GRADING2A 3.b)\n");
                int create_ret = dir_vnode->vn_ops->mknod(dir_vnode, filename, namelen, mode, (devid_t) devid);
                vput(dir_vnode);
                if (create_ret < 0){
                        dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() with error: error happens in AFS\n");
                        return create_ret;
                }
                //suceess
                dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() successfully with ret val %d\n", create_ret);
                return create_ret;
        }
        //NOT_YET_IMPLEMENTED("VFS: do_mknod");
        vput(dir_vnode);
        dbg(DBG_PRINT, "(GRADING2D) leave do_mknod() with error: other errors occurs, with error number %d\n", ret);
        return ret;
}

/* Use dir_namev() to find the vnode of the dir we want to make the new
 * directory in.  Then use lookup() to make sure it doesn't already exist.
 * Finally call the dir's mkdir vn_ops. Return what it returns.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EEXIST
 *        path already exists.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_mkdir(const char *path)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_mkdir()\n");
        size_t namelen;
        const char * filename;
        vnode_t * dir_vnode;
        vnode_t * file_vnode;

        int ret = dir_namev(path, &namelen, &filename, NULL, &dir_vnode);

        if (ret < 0){//ENOENT || ENAMETOOLONG
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: error number return from dir_namev()\n");
                return ret;                
        }
        

        ret = lookup(dir_vnode, filename, namelen, &file_vnode);
        
        if (ret == 0){//lookup success, filename exists
                vput(file_vnode);
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: path already exists\n");
                return -EEXIST;
        }
        if (ret == -ENOTDIR){
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: file_vnode's base directory is not directory\n");
                return -ENOTDIR;
        }
        if (ret == -ENAMETOOLONG){
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: file_vnode name in path is too long\n");
                return -ENAMETOOLONG;
        }
        if (ret == -ENOENT){ /* dir_vnode is the directory vnode where you will create the target special file */
                KASSERT(NULL != dir_vnode->vn_ops->mkdir);
                dbg(DBG_PRINT, "(GRADING2A 3.c)\n");
                int create_ret = dir_vnode->vn_ops->mkdir(dir_vnode, filename, namelen);
                vput(dir_vnode);
                if (create_ret < 0){
                        dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: error happens in AFS\n");
                        return create_ret;
                }
                //suceess
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() successfully with ret val %d\n",create_ret);
                return create_ret;
        }
        //NOT_YET_IMPLEMENTED("VFS: do_mkdir");
        vput(dir_vnode);
        dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: other errors occurs, with error number %d\n", ret);
        return ret;
}

/* Use dir_namev() to find the vnode of the directory containing the dir to be
 * removed. Then call the containing dir's rmdir v_op.  The rmdir v_op will
 * return an error if the dir to be removed does not exist or is not empty, so
 * you don't need to worry about that here. Return the value of the v_op,
 * or an error.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EINVAL
 *        path has "." as its final component.
 *      o ENOTEMPTY
 *        path has ".." as its final component.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_rmdir(const char *path)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_rmdir()\n");
        size_t namelen;
        const char * filename;
        vnode_t * dir_vnode;

        int ret = dir_namev(path, &namelen, &filename, NULL, &dir_vnode);

        if (ret < 0){//ENOENT || ENAMETOOLONG || ENOTDIR
                dbg(DBG_PRINT, "(GRADING2D) leave do_rmdir() with error: error number return from dir_namev()\n");
                return ret;
        }
        if (name_match(".", filename, namelen)){
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: '.' is its final component\n");
                return -EINVAL;
        }
        if (name_match("..", filename, namelen)){
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: '..' is its final component\n");
                return -ENOTEMPTY;
        }
        if (!S_ISDIR(dir_vnode->vn_mode)){
                vput(dir_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_mkdir() with error: dir_vnode is not a directory\n");
                return -ENOTDIR;
        }
        /* dir_vnode is the directory vnode where you will remove the target directory */
        KASSERT(NULL != dir_vnode->vn_ops->rmdir);
        dbg(DBG_PRINT, "(GRADING2A 3.d)\n");
        dbg(DBG_PRINT, "(GRADING2B)\n");
        ret = dir_vnode->vn_ops->rmdir(dir_vnode, filename, namelen);
        vput(dir_vnode);
        dbg(DBG_PRINT, "(GRADING2D) leave do_rmdir() with ret value %d\n",ret);
        //NOT_YET_IMPLEMENTED("VFS: do_rmdir");
        return ret;
}

/*
 * Similar to do_rmdir, but for files.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EPERM
 *        path refers to a directory.
 *      o ENOENT
 *        Any component in path does not exist, including the element at the
 *        very end.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int
do_unlink(const char *path)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_unlink()\n");
        size_t namelen;
        const char * filename;
        vnode_t * dir_vnode;

        int ret = dir_namev(path, &namelen, &filename, NULL, &dir_vnode);

        if (ret < 0){//ENOENT || ENAMETOOLONG || ENOTDIR
                dbg(DBG_PRINT, "(GRADING2D) leave do_unlink() with error: error number return from dir_namev()\n");
                return ret;
        }
        vnode_t * file_vnode;

        ret = lookup(dir_vnode, filename, namelen, &file_vnode);

        if (ret < 0){//ENODIR
                vput(dir_vnode);
                dbg(DBG_PRINT,"(GRADING2D) leave do_unlink() with error: error number return form lookup().\n");
                return ret;
        }
        if (S_ISDIR(file_vnode->vn_mode)){
                vput(dir_vnode);
                vput(file_vnode);
                dbg(DBG_PRINT,"(GRADING2D) leave do_unlink() with error: file_vnode is a directory rather than a file.\n");
                return -EPERM;
        }
        /* dir_vnode is the directory vnode where you will unlink the target file */
        KASSERT(NULL != dir_vnode->vn_ops->unlink);
        dbg(DBG_PRINT, "(GRADING2A 3.e)\n");
        dbg(DBG_PRINT, "(GRADING2B)\n");
        ret = dir_vnode->vn_ops->unlink(dir_vnode, filename, namelen);
        vput(dir_vnode);
        vput(file_vnode);
        dbg(DBG_PRINT, "(GRADING2D) leave do_unlink() with ret val %d.\n",ret);
        //NOT_YET_IMPLEMENTED("VFS: do_unlink");
        return ret;
}

/* To link:
 *      o open_namev(from)
 *      o dir_namev(to)
 *      o call the destination dir's (to) link vn_ops.
 *      o return the result of link, or an error
 *
 * Remember to vput the vnodes returned from open_namev and dir_namev.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EEXIST
 *        to already exists.
 *      o ENOENT
 *        A directory component in from or to does not exist.
 *      o ENOTDIR
 *        A component used as a directory in from or to is not, in fact, a
 *        directory.
 *      o ENAMETOOLONG
 *        A component of from or to was too long.
 *      o EPERM
 *        from is a directory.
 */
int
do_link(const char *from, const char *to)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_link()\n");
        vnode_t * from_node;
        vnode_t * to_dir_node;
        vnode_t * to_file_node;
        int ret = open_namev(from, O_RDONLY, &from_node, NULL);

        if (ret < 0){
                dbg(DBG_PRINT, "(GRADING2D) leave do_link() with error: error happens in open_namev(from)\n");
                return ret;
        }
        if (S_ISDIR(from_node->vn_mode)){
                vput(from_node);
                dbg(DBG_PRINT, "(GRADING2D) leave do_link() with error: from is a directory\n");
                return -EPERM;
        }
        //from ok
        size_t namelen;
        const char * filename;
        ret = dir_namev(to, &namelen, &filename, NULL, &to_dir_node);

        if (ret < 0){
                vput(from_node);
                dbg(DBG_PRINT, "(GRADING2D) leave do_link() with error: error happens in dir_namev(to_dir)\n");
                return ret;
        }
        if (!S_ISDIR(to_dir_node->vn_mode)){
                vput(from_node);
                vput(to_dir_node);
                dbg(DBG_PRINT, "(GRADING2D) leave do_link() with error: to_dir_node is not a directory \n");
                return -ENOTDIR;
        }
        //to_dir part ok

        ret = lookup(to_dir_node, filename, namelen, &to_file_node);
        if (ret == 0){//to already exists
                vput(from_node);
                vput(to_dir_node);
                vput(to_file_node);
                dbg(DBG_PRINT, "(GRADING2D) leave do_link() with error: to file already exists\n");
                return -EEXIST;
        }
        //ENOENT
        ret = to_dir_node->vn_ops->link(from_node, to_dir_node, filename, namelen);
        vput(from_node);
        vput(to_dir_node);
        //vput(to_file_node);
        dbg(DBG_PRINT, "(GRADING2D) leave do_link() successfully\n");
        //NOT_YET_IMPLEMENTED("VFS: do_link");
        return ret;
}

/*      o link newname to oldname
 *      o unlink oldname
 *      o return the value of unlink, or an error
 *
 * Note that this does not provide the same behavior as the
 * Linux system call (if unlink fails then two links to the
 * file could exist).
 */
int
do_rename(const char *oldname, const char *newname)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_rename()\n");
        int ret = do_link(oldname, newname);
        if (ret < 0){
                dbg(DBG_PRINT, "(GRADING2D) leave do_reaname() with error: error happens in do_link()\n");
                return ret;
        }
        ret = do_unlink(oldname);
        dbg(DBG_PRINT, "(GRADING2D) leave do_rename() with return value %d\n", ret);
        //NOT_YET_IMPLEMENTED("VFS: do_rename");
        return ret;
}

/* Make the named directory the current process's cwd (current working
 * directory).  Don't forget to down the refcount to the old cwd (vput()) and
 * up the refcount to the new cwd (open_namev() or vget()). Return 0 on
 * success.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o ENOENT
 *        path does not exist.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 *      o ENOTDIR
 *        A component of path is not a directory.
 */
int
do_chdir(const char *path)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_chdir()\n");
        vnode_t * from_vnode = curproc->p_cwd;
        vnode_t * to_vnode;

        int ret = open_namev(path, O_RDONLY, &to_vnode, NULL);
        if (ret < 0){//ENOENT || ENAMETOOLONG
                dbg(DBG_PRINT, "(GRADING2D) leave do_chdir() with error: error happens in open_namev()\n");
                return ret;
        }
        if (!S_ISDIR(to_vnode->vn_mode)){
                vput(to_vnode);
                dbg(DBG_PRINT, "(GRADING2D) leave do_chdir() with error: to_vnode is not a directory\n");
                return -ENOTDIR;
        }
        vput(from_vnode);
        curproc->p_cwd = to_vnode;
        dbg(DBG_PRINT, "(GRADING2D) leave do_chidir() successfully\n");
        //NOT_YET_IMPLEMENTED("VFS: do_chdir");
        return 0;
}

/* Call the readdir vn_op on the given fd, filling in the given dirent_t*.
 * If the readdir vn_op is successful, it will return a positive value which
 * is the number of bytes copied to the dirent_t.  You need to increment the
 * file_t's f_pos by this amount.  As always, be aware of refcounts, check
 * the return value of the fget and the virtual function, and be sure the
 * virtual function exists (is not null) before calling it.
 *
 * Return either 0 or sizeof(dirent_t), or -errno.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        Invalid file descriptor fd.
 *      o ENOTDIR
 *        File descriptor does not refer to a directory.
 */
int
do_getdent(int fd, struct dirent *dirp)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_getdent()\n");

        if (fd < 0 || fd > NFILES ){
                dbg(DBG_PRINT, "(GRADING2D) leave do_chidir() with error: not valid file descriptor, return -EBADF\n");
                return -EBADF;
        }

        file_t * file = fget(fd);
        if (file == NULL){
                dbg(DBG_PRINT, "(GRADING2D) leave do_chidir() with error: not valid file descriptor, return -EBADF\n");
                return -EBADF;
        }
        if (!S_ISDIR(file->f_vnode->vn_mode)){
                fput(file);
                dbg(DBG_PRINT, "(GRADING2D) leave do_chidir() with error: fd is not refer to a directory, return -ENOTDIR\n");
                return -ENOTDIR;
        }
        //ret is the number of bytes copied to the dirent_t
        int ret = file->f_vnode->vn_ops->readdir(file->f_vnode, file->f_pos, dirp);
        if (ret == 0){//If the end of the file as been reached (offset == file->vn_len), no directory entry will be read and 0 will be returned
                dbg(DBG_PRINT, "(GRADING2D) leave do_getdent() with end of file reached, return 0\n");
                fput(file);
                return ret;
        }
        
        do_lseek(fd, ret, SEEK_CUR);
        fput(file);

        //NOT_YET_IMPLEMENTED("VFS: do_getdent");
        dbg(DBG_PRINT, "(GRADING2D) leave do_getdent() successfully\n");
        return sizeof(dirent_t);
}

/*
 * Modify f_pos according to offset and whence.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not an open file descriptor.
 *      o EINVAL
 *        whence is not one of SEEK_SET, SEEK_CUR, SEEK_END; or the resulting
 *        file offset would be negative.
 */
int
do_lseek(int fd, int offset, int whence)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_lseek()\n");
        if (fd < 0 || fd > NFILES){
                dbg(DBG_PRINT, "(GRADING2D) leave do_lseek() with error: not valid file descriptor, return -EBADF\n");
                return -EBADF;
        }
        file_t * file = fget(fd);
        if (file == NULL){
                dbg(DBG_PRINT, "(GRADING2D) leave do_lseek() with error: not valid file descriptor, return -EBADF\n");
                return -EBADF;
        }
        int f_offset;
        switch(whence){
                case SEEK_SET:
                        f_offset = offset;
                        break;
                case SEEK_CUR:
                        f_offset = file->f_pos + offset;
                        break;
                case SEEK_END:
                        f_offset = file->f_vnode->vn_len + offset;
                        break;
                default:
                        dbg(DBG_PRINT, "(GRADING2D) leave do_lseek() with error: whence value is not valid\n");
                        fput(file);
                        return -EINVAL;
        }
        if(f_offset < 0){
                fput(file);
                dbg(DBG_PRINT, "(GRADING2D) leave do_lseek() with error: file offset is negative, return -EINVAL\n");
                return -EINVAL;
        }
        file->f_pos = f_offset;
        fput(file);
        //NOT_YET_IMPLEMENTED("VFS: do_lseek");
        dbg(DBG_PRINT, "(GRADING2D) leave do_lseek() successfully, return val: %d\n",f_offset);
        return f_offset;
}

/*
 * Find the vnode associated with the path, and call the stat() vnode operation.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o ENOENT
 *        A component of path does not exist.
 *      o ENOTDIR
 *        A component of the path prefix of path is not a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 *      o EINVAL
 *        path is an empty string.
 */
int
do_stat(const char *path, struct stat *buf)
{
        dbg(DBG_PRINT, "(GRADING2D) enter do_stat()\n");
        vnode_t * f_vnode;
        int ret = open_namev(path, O_RDONLY, &f_vnode, NULL);
        if (ret < 0){//ENOENT
                dbg(DBG_PRINT, "(GRADING2D) leave do_stat() with error, error happens in open_namev(), ret val is %d\n",ret);
                return ret;
        }
        KASSERT(NULL != f_vnode->vn_ops->stat);
        dbg(DBG_PRINT, "(GRADING2A 3.f)\n");
        dbg(DBG_PRINT, "(GRADING2B)\n");

        ret = f_vnode->vn_ops->stat(f_vnode, buf);
        vput(f_vnode);
        //NOT_YET_IMPLEMENTED("VFS: do_stat");
        dbg(DBG_PRINT, "(GRADING2D) leave do_stat() successfully\n");
        return ret;
}

#ifdef __MOUNTING__
/*
 * Implementing this function is not required and strongly discouraged unless
 * you are absolutely sure your Weenix is perfect.
 *
 * This is the syscall entry point into vfs for mounting. You will need to
 * create the fs_t struct and populate its fs_dev and fs_type fields before
 * calling vfs's mountfunc(). mountfunc() will use the fields you populated
 * in order to determine which underlying filesystem's mount function should
 * be run, then it will finish setting up the fs_t struct. At this point you
 * have a fully functioning file system, however it is not mounted on the
 * virtual file system, you will need to call vfs_mount to do this.
 *
 * There are lots of things which can go wrong here. Make sure you have good
 * error handling. Remember the fs_dev and fs_type buffers have limited size
 * so you should not write arbitrary length strings to them.
 */
int
do_mount(const char *source, const char *target, const char *type)
{
        NOT_YET_IMPLEMENTED("MOUNTING: do_mount");
        return -EINVAL;
}

/*
 * Implementing this function is not required and strongly discouraged unless
 * you are absolutley sure your Weenix is perfect.
 *
 * This function delegates all of the real work to vfs_umount. You should not worry
 * about freeing the fs_t struct here, that is done in vfs_umount. All this function
 * does is figure out which file system to pass to vfs_umount and do good error
 * checking.
 */
int
do_umount(const char *target)
{
        NOT_YET_IMPLEMENTED("MOUNTING: do_umount");
        return -EINVAL;
}
#endif
