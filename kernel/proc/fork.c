/******************************************************************************/
/* Important Spring 2020 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5fd1e93dbf35cbffa3aef28f8c01d8cf2ffc51ef62b26a       */
/*         f9bda5a68e5ed8c972b17bab0f42e24b19daa7bd408305b1f7bd6c7208c1       */
/*         0e36230e913039b3046dd5fd0ba706a624d33dbaa4d6aab02c82fe09f561       */
/*         01b0fd977b0051f0b0ce0c69f7db857b1b5e007be2db6d42894bf93de848       */
/*         806d9152bd5715e9                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "errno.h"

#include "util/debug.h"
#include "util/string.h"

#include "proc/proc.h"
#include "proc/kthread.h"

#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/page.h"
#include "mm/pframe.h"
#include "mm/mmobj.h"
#include "mm/pagetable.h"
#include "mm/tlb.h"

#include "fs/file.h"
#include "fs/vnode.h"

#include "vm/shadow.h"
#include "vm/vmmap.h"

#include "api/exec.h"

#include "main/interrupt.h"

/* Pushes the appropriate things onto the kernel stack of a newly forked thread
 * so that it can begin execution in userland_entry.
 * regs: registers the new thread should have on execution
 * kstack: location of the new thread's kernel stack
 * Returns the new stack pointer on success. */
static uint32_t
fork_setup_stack(const regs_t *regs, void *kstack)
{
        /* Pointer argument and dummy return address, and userland dummy return
         * address */
        uint32_t esp = ((uint32_t) kstack) + DEFAULT_STACK_SIZE - (sizeof(regs_t) + 12);
        *(void **)(esp + 4) = (void *)(esp + 8); /* Set the argument to point to location of struct on stack */
        memcpy((void *)(esp + 8), regs, sizeof(regs_t)); /* Copy over struct */
        return esp;
}


/*
 * The implementation of fork(2). Once this works,
 * you're practically home free. This is what the
 * entirety of Weenix has been leading up to.
 * Go forth and conquer.
 */
int
do_fork(struct regs *regs)
{
        dbg(DBG_PRINT, "(GRADING3F) enter do_fork()\n");
        //(let newproc be a pointer to the new child process and let newthr be the thread in newproc)
        //vmarea_t *vma = NULL, *clone_vma = NULL;
        //pframe_t *pf;
        //mmobj_t *to_delete, *new_shadowed;

        KASSERT(regs != NULL); /* the function argument must be non-NULL */
        KASSERT(curproc != NULL); /* the parent process, which is curproc, must be non-NULL */
        KASSERT(curproc->p_state == PROC_RUNNING); /* the parent process must be in the running state and not in the zombie state */
        dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

        //1. Allocate a proc_t out of the proc structure using proc_create()
        proc_t * newproc = proc_create(curproc->p_comm);
        KASSERT(newproc->p_state == PROC_RUNNING); /* new child process starts in the running state */
        dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

        /*2. Copy the vmmap_t from the parent process into the child using vmmap_clone().
        Remember to increase the refernece counts on the underlying mmobj_ts*/
        newproc->p_vmmap = vmmap_clone(curproc->p_vmmap);
        newproc->p_vmmap->vmm_proc = newproc;

        /*3. For each private mapping, point the vmarea_t at the new shadow object, which in turn should point to the original mmobj_t for the vmarea_t
        This is how you know that the pages corresponding to this mapping are copy-on-write. 
        Be careful with refernce counts. Also note that for shared mappings, there is no need to copy the mmobj_t.*/

        vmarea_t * child_vma;
        list_iterate_begin(&(newproc->p_vmmap->vmm_list), child_vma, vmarea_t, vma_plink){
            vmarea_t *vma = vmmap_lookup(curproc->p_vmmap, child_vma->vma_start);
            if (vma != NULL){
                int mapping_type = child_vma->vma_flags & MAP_TYPE;

                if (mapping_type & MAP_PRIVATE){//private
                    mmobj_t *new_parent_shadowed = shadow_create();
                    mmobj_t *new_child_shadowed = shadow_create();

                    vma->vma_obj->mmo_ops->ref(vma->vma_obj);

                    new_child_shadowed->mmo_shadowed = vma->vma_obj;
                    new_parent_shadowed->mmo_shadowed = vma->vma_obj;

                    new_child_shadowed->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(vma->vma_obj);
                    new_parent_shadowed->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(vma->vma_obj);

                    vma->vma_obj = new_parent_shadowed;
                    child_vma->vma_obj = new_child_shadowed;
                    dbg(DBG_PRINT,"(GRADING3F) private mapping, create 2 shadow objs and relink\n");
                }
                else if (mapping_type & MAP_SHARED){//shared
                    child_vma->vma_obj = vma->vma_obj;
                    child_vma->vma_obj->mmo_ops->ref(child_vma->vma_obj);
                    dbg(DBG_PRINT,"(GRADING3F) shared mapping, relink\n");
                }
                
                list_insert_tail(mmobj_bottom_vmas(child_vma->vma_obj), &child_vma->vma_olink);
            }
        }list_iterate_end();

        /*4. Unmap the user land page table entries and flush the TLB(using pt_unmap_range() and tlb_flush_all())*/
        pt_unmap_range(curproc->p_pagedir, USER_MEM_LOW, USER_MEM_HIGH);
        pt_unmap_range(newproc->p_pagedir, USER_MEM_LOW, USER_MEM_HIGH);
        tlb_flush_all();

        KASSERT(newproc->p_pagedir != NULL); /* new child process must have a valid page table */
        dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

        //5. Set up the new process thread context(kt_ctx) and clone newthr from curthr
        //find the correct thread


        kthread_t* newthr = NULL;
        kthread_t* oldthr = NULL;
        kthread_t* runnewthr = NULL;
        
        regs->r_eax = 0;
        regs->r_err = 0;
        
        list_iterate_begin(&curproc->p_threads, oldthr, kthread_t, kt_plink){
            newthr = kthread_clone(oldthr);
            KASSERT(newthr->kt_kstack != NULL); /* thread in the new child process must have a valid kernel stack */
            dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

            newthr->kt_proc = newproc;
            
            newthr->kt_ctx.c_esp = fork_setup_stack(regs, (void *)newthr->kt_kstack);
            newthr->kt_ctx.c_ebp = newthr->kt_ctx.c_esp;
            newthr->kt_ctx.c_eip = (uint32_t) userland_entry;
            newthr->kt_ctx.c_pdptr = newproc->p_pagedir;

            list_insert_tail(&newproc->p_threads, &newthr->kt_plink);

            //find the runnable new thread
            if (curthr == oldthr){
               runnewthr = newthr;
            }
        }list_iterate_end();

        //6. Copy the file descriptor table of the parent into the child. Remeber to use fref() here
        int i;
        for(i = 0; i < NFILES; i++){
            newproc->p_files[i] = curproc->p_files[i];
            if (newproc->p_files[i] != NULL){
                fref(newproc->p_files[i]);
                //refcount of open fd increment
            }
        }

        //7. Set the child's working directory to point to the parent's working directory
        //(once again, remember refernce counts)
        newproc->p_cwd = curproc->p_cwd;

        //8. Set any other fields in the new process which need to be set
        newproc->p_brk = curproc->p_brk;
        newproc->p_start_brk = curproc->p_start_brk;

        //9. Make runnable newthr
        sched_make_runnable(runnewthr);
        //NOT_YET_IMPLEMENTED("VM: do_fork");

        //we need to clarify the returning proc is the cur or the child
        dbg(DBG_PRINT,"GRADING3F: leave do_fork() successfully, return newproc's pid: %d\n", newproc->p_pid);

        return newproc->p_pid;
}
